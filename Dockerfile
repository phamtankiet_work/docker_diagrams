FROM python:3.9-slim

RUN apt-get update && apt-get install -y graphviz
RUN pip install watchdog diagrams

# Set working directory
WORKDIR /work_dir_temp

# Copy the Python script into the container
COPY reloader.py .

# Run the Python script
CMD ["python", "reloader.py"]