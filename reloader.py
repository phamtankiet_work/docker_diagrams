import subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import os
os.chdir("/work_dir")

class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        if event.src_path.endswith('.py'):
            print("Recompiling {}".format(event.src_path))
            result = subprocess.run(
                ['python', event.src_path], capture_output=True)
            if result.returncode == 0:
                print("Compilation successful.")
            else:
                print("Compilation failed with output:")
                print(result.stderr.decode('utf-8'))


if __name__ == "__main__":
    observer = Observer()
    observer.schedule(MyHandler(), path='.', recursive=True)
    observer.start()
    print("Watching for changes...")

try:
    while True:
        pass
except KeyboardInterrupt:
    observer.stop()

observer.join()
